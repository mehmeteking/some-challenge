package com.example
package challenge

import LinkedList._

object LinkedListUtils {

  def reverseIteratively[A](list: LinkedList[A]) = {
    var curr = list
    var newList: LinkedList[A] = Empty

    while (curr != Empty) {
      newList = newList.prepend(curr.head)
      curr = curr.tail
    }
    newList
  }

  def reverseRecursively[A](list: LinkedList[A]): LinkedList[A] = reverseRecursively(Empty, list)

  @scala.annotation.tailrec
  private[this] def reverseRecursively[A](newList: LinkedList[A], list: LinkedList[A]): LinkedList[A] = {
    if (list == Empty) return newList
    reverseRecursively(newList.prepend(list.head), list.tail)
  }

}
