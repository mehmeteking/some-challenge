package com.example
package challenge

trait LinkedList[+A] {

  def head: A
  def tail: LinkedList[A]

  def prepend[A1 >: A](item: A1): LinkedList[A1]
  def append[A1 >: A](item: A1): LinkedList[A1]

}
object LinkedList {

  case class Node[A](head: A, tail: LinkedList[A]) extends LinkedList[A] {

    def prepend[A1 >: A](item: A1) = new Node[A1](item, this)
    def append[A1 >: A](item: A1): LinkedList[A1] = new Node(head, tail.append[A1](item))

  }
  object Node

  case object Empty extends LinkedList[Nothing] {

    def head = throw new NoSuchElement
    def tail = throw new NoSuchElement

    def prepend[A1 >: Nothing](item: A1) = new Node[A1](item, this)
    def append[A1 >: Nothing](item: A1) = new Node[A1](item, this)

  }

  class NoSuchElement extends Exception

}
