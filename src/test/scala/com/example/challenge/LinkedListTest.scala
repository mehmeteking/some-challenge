package com.example
package challenge

import LinkedList._
import TestHelpers._

class LinkedListTest extends Spec {

  "Empty" >> {
    val list: LinkedList[Any] = Empty
    val newElement = 5

    "head" >> {
      "throw NoSuchElementException" in {
        list.head must throwA[NoSuchElement]
      }
    }

    "tail" >> {
      "throw NoSuchElementException" in {
        list.tail must throwA[NoSuchElement]
      }
    }

    "append(A)" >> {
      "create new list with single element" in {
        list.append(newElement) must beSameAs(Seq(newElement))
      }
    }

    "prepend(A)" >> {
      "create new list with single element" in {
        list.prepend(newElement) must beSameAs(Seq(newElement))
      }
    }
  }

  "Node" >> {
    val head = 3
    val tail = LinkedList.Node(5, Empty)
    val newElement = 8
    val list: LinkedList[Any] = Node(head, tail)

    "head" >> {
      "return head element" in {
        list.head must beEqualTo(head)
      }
    }

    "tail" >> {
      "return tail list" in {
        list.tail must beEqualTo(tail)
      }
    }

    "append(A)" >> {
      val appendedList = list.append(newElement)
      val appendedSeq = Seq(list.head, list.tail.head, newElement)

      "append element to list" in {
        appendedList must beSameAs(appendedSeq)
      }
    }

    "prepend(A)" >> {
      val prependedList = list.prepend(newElement)
      val prependedSeq = Seq(newElement, list.head, list.tail.head)

      "prepend element to list" in {
        prependedList must beSameAs(prependedSeq)
      }
    }
  }

}
