package com.example
package challenge

import LinkedList._
import LinkedListUtils._
import TestHelpers._

class LinkedListUtilsTest extends Spec {
  type ListOp[A] = LinkedList[A] => LinkedList[A]

  "reverse iteratively" >> {
    checkReverseOp(reverseIteratively)
  }

  "reverse recursively" >> {
    checkReverseOp(reverseRecursively)
  }

  def checkReverseOp(op: ListOp[Int]) = {
    val seq = Seq(3, 5, 8, 13)
    var temp: LinkedList[Int] = Empty
    for (i <- seq.reverse) temp = Node(i, temp)
    val list = temp

    "reverse list" >> {
      "when empty, return empty" in {
        op(Empty) must beEqualTo(Empty)
      }
      "when populated, return reverse" in {
        op(list) must beSameAs(seq.reverse)
      }
    }
  }

}
