package com.example
package challenge

import org.specs2.matcher.{ Expectable, Matcher }
import org.specs2.mutable.Specification

trait TestHelpers {

  trait Spec extends Specification
  object Spec

  def beSameAs[A](seq: Seq[A]): Matcher[LinkedList[A]] = new LinkedListSeqMatcher(seq)

  @scala.annotation.tailrec
  private def beSameAs[A](list: LinkedList[A], seq: Seq[A]): Boolean =
    if (list == LinkedList.Empty && seq.isEmpty) true
    else if (list.head != seq.head) false
    else beSameAs(list.tail, seq.tail)

  class LinkedListSeqMatcher[A](seq: Seq[A]) extends Matcher[LinkedList[A]] {
    def apply[L <: LinkedList[A]](list: Expectable[L]) = {
      result(beSameAs(list.value, seq),
        list.description + s" is same as $seq",
        list.description + s" is not same as $seq",
        list)
    }
  }

}
object TestHelpers extends TestHelpers
