name := "amazon-challenge"

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "org.specs2" %% "specs2-core" % "3.5" % Test,
  "org.specs2" %% "specs2-mock" % "3.5" % Test
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
